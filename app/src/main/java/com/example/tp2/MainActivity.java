package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    protected static WineDbHelper DBWINE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DBWINE = new WineDbHelper(getApplicationContext());
        DBWINE.populate();

        Cursor listWine = DBWINE.fetchAllWines();

        ListView WINE = findViewById(R.id.listvin);
        SimpleCursorAdapter ADAPTER = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, listWine,
                new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2}, 0);
        WINE.setAdapter(ADAPTER);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newWine = new Intent(MainActivity.this, WineActivity.class);
                newWine.putExtra("WINE_CLICK", new Wine());
                startActivity(newWine);
            }
        });

        WINE.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("WINE_CLICK", WineDbHelper.cursorToWine((Cursor)parent.getItemAtPosition(position)));
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        Cursor listWine = DBWINE.fetchAllWines();
        ListView WINE = findViewById(R.id.listvin);
        SimpleCursorAdapter ADAPTER = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, listWine,
                new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2}, 0);
        WINE.setAdapter(ADAPTER);

    }
}
