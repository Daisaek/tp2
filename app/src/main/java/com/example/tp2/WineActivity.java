package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import static com.example.tp2.MainActivity.DBWINE;

public class WineActivity extends AppCompatActivity {
    EditText wineName;
    EditText editWineRegion;
    EditText editLoc;
    EditText editClimate;
    EditText editPlantedArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        wineName = (EditText) findViewById(R.id.wineName);
        editWineRegion = (EditText) findViewById(R.id.editWineRegion);
        editLoc = (EditText) findViewById(R.id.editLoc);
        editClimate = (EditText) findViewById(R.id.editClimate);
        editPlantedArea = (EditText) findViewById(R.id.editPlantedArea);

        Intent data = getIntent();

        final Wine wine = data.getExtras().getParcelable("WINE_CLICK");

        wineName.setText(wine.getTitle());
        editWineRegion.setText(wine.getRegion());
        editLoc.setText(wine.getLocalization());
        editClimate.setText(wine.getClimate());
        editPlantedArea.setText(wine.getPlantedArea());

        Button button = (Button)findViewById(R.id.save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine upWine = new Wine(
                        wineName.getText().toString(),
                        editWineRegion.getText().toString(),
                        editLoc.getText().toString(),
                        editClimate.getText().toString(),
                        editPlantedArea.getText().toString()
                );
                DBWINE.updateWine(upWine);
                finish();
            }
        });
    }
}
